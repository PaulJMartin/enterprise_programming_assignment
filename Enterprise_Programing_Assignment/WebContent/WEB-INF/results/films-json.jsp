<%@ page import="java.util.List"%>
<%@ page import="coreClasses.Film"%>
<%@ page import="com.google.gson.Gson"%>

<%
	// suppresses compile warnings about unchecked generic operations.
	@SuppressWarnings("unchecked")
	// Retrives the "Films" request attribute from the servlet and parses it into the array list of type film.
	// This array list now holds the film objects sent from the servlet.
	List<Film> filmList = (List<Film>) request.getAttribute("Films");
	// Isntiating a new Gson object.
	Gson gson = new Gson();
	// The array list of films is then passed into the parameter of the toJson method on the Gson object, which parses the array list of films as a JSON string.
	// The JSON string is then passed to the toJson string variable.
	String toJson = gson.toJson(filmList);
	// Sends the JSON string back to the servlet.
	out.println(toJson);
	// Prints the JSON string to the console for debugging purposes.
	System.out.println(toJson);
%>