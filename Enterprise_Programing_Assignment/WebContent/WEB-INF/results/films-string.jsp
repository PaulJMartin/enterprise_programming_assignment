<%@ page import="java.util.List"%>
<%@ page import="coreClasses.Film"%>


<%
	//suppresses compile warnings about unchecked generic operations.
	@SuppressWarnings("unchecked")
	//Retrives the "Films" request attribute from the servlet and parses it into the array list of type film.
	//This array list now holds the film objects sent from the servlet.
	List<Film> filmList = (List<Film>) request.getAttribute("Films");
	// Null film object is constructed for use within the for loop.
	Film film = null;
	// Empty String variable is constructed for use within the for loop.
	String filmStr = "";
	// Loops through the filmList array list.
	for (int i = 0; i < filmList.size(); i++) {
		// The film object at the specified index is parsed into the film object.
		film = filmList.get(i);
		// The film classes toStringAjax method is used on the film object here, to add the delimiters around the attributes and at the end of the film string.
		// This is so the string data can be split within the AJAX calls on the client side.
		filmStr += film.toStringAjax();
	}
	//Prints the text string to the console for debugging purposes.
	out.println(filmStr);
%>