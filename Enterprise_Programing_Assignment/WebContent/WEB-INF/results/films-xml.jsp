<%@ page trimDirectiveWhitespaces="true"%>
<%@ page import="coreClasses.Film"%>
<%@ page import="coreClasses.FilmList"%>
<%@ page import="coreClasses.FilmDAO"%>
<%@ page import="javax.xml.bind.JAXBContext"%>
<%@ page import="javax.xml.bind.Marshaller"%>
<%@ page import="java.io.StringWriter"%>
<%@ page import="javax.xml.bind.JAXBException"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>

<%
	// suppresses compile warnings about unchecked generic operations.
	@SuppressWarnings("unchecked")
	// Retrives the "Films" request attribute from the servlet and parses it into the array list of type film.
	// This array list now holds the film objects sent from the servlet.
	FilmList films = new FilmList((List<Film>) request.getAttribute("Films"));

	try {
		// Instantiates a new JAXBContext object, using the FilmList class as the class to be bound by the new JAXBContext.
		JAXBContext jaxbContext = JAXBContext.newInstance(FilmList.class);
		// Instantiating a Marshaller object which is used to convert a Java content tree into XML data
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// Formats the XML document.
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		// Sends the XML document back to the servlet.
		jaxbMarshaller.marshal(films, out);

	} catch (JAXBException e) {
		e.printStackTrace();
	}
%>

