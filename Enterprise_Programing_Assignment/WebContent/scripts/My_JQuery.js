// This function is a pre-set JQueryUI function which builds a button widget.
$(function() {
	$(".widget input[type=submit], .widget a, .widget button").button();
	$("button, input, a").click(function(event) {
		event.preventDefault();
	})
});

// This function is a pre-set JQueryUI function which builds a dropdown widget
// for the all films format selection.
$(function() {
	$("#AllFilmsformat").selectmenu().selectmenu("menuWidget");
});

// This function is a pre-set JQueryUI function which builds a dropdown widget
// for the film like title film format selection.
$(function() {
	$("#format").selectmenu().selectmenu("menuWidget");
});