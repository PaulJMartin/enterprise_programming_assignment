// These are the instance variables used within the AJAX file.
var filmID = "Film ID";
var filmTitle = "Title";
var filmYear = "Year";
var filmDirector = "Director";
var filmStars = "Stars";
var filmReview = "Review";

// Code included inside $( document ).ready() will only run once the page
// Document Object Model (DOM) is ready for JavaScript code to execute.
// This runs the anonymous function within the .ready method.

$(document).ready(function() {
});

// This is the function which is run when the document state is ready, it checks
// if any of the buttons within the XHTML page have been selected.

$(function() {

	// Loads function within the click method, when the button with the ID of
	// "returnAllFilms" is selected.
	$("#returnAllFilms").click(function() {
		// Finds the value with the dropdown list with the ID of "format".
		var format = $("#format").val();
		// Executes the loadAllData function with the format variable as its
		// parameter.
		loadAllData(format);
	});

	// Loads function within the click method, when the button with the ID of
	// "returnFilmLikeTitle" is selected.
	$("#returnFilmLikeTitle").click(function() {
		// Finds the value with the dropdown list with the ID of
		// "AllFilmsformat".
		var format = $("#AllFilmsformat").val();
		// Executes the loadData function with the format variable as its
		// parameter.
		loadData(format);
	});

	// Loads function within the click method, when the button with the ID of
	// "insertFilm" is selected.
	$("#insertFilm").click(function() {
		// Executes the insertData function
		insertData();
	});

	// Loads function within the click method, when the button with the ID of
	// "deleteFilm" is selected.
	$("#deleteFilm").click(function() {
		// Executes the deleteData function
		deleteData();
	});

	// Loads function within the click method, when the button with the ID of
	// "updateFilmName" is selected.
	$("#updateFilmName").click(function() {
		// Executes the updateFilmName function
		updateFilmName();
	});

});

// Loaded when the "returnAllFilms" button is selected.
function loadAllData(format) {
	// Starts the AJAX call.
	$.ajax({
		// The URL is equal to the "GetAllFilms" servlet.
		url : "GetAllFilms",
		// The data is equal to all the data entered into the "getAllFilmsForm"
		data : $("#getAllFilmsForm").serialize(),
		// If the operation is successful, the anonymous inner function is
		// called and the result of the request is passed into the functions
		// parameter.
		success : function(result) {
			// This is here for testing purposes, to show the result of the
			// operation.
			console.log(result);

			// This if statement checks to see if the format variable is == XML.
			// If it is equal to XML the body of the statement is executed.
			if (format == "xml") {
				// The xmlData function with a parameter of "result" is called
				// within the HTML method attached to the JQuery call.
				// This returns passes in an XML document to the xmlData
				// function, which takes the data and converts it to XHTML table
				// data and returns it to the HTML method.
				// This in turn pushes the table data into the divider within
				// the XHTML page with the ID of "AllFilms-div".
				$("#AllFilms-div").html(xmlData(result));
			}
			// This else if statement checks to see if the format variable is ==
			// text. If it is equal to text the body of the statement is
			// executed.
			else if (format == "text") {
				// The textData function with a parameter of "result" is called
				// within the HTML method attached to the JQuery call.
				// This returns passes in an text document to the textData
				// function, which takes the data and converts it to XHTML table
				// data and returns it to the HTML method.
				// This in turn pushes the table data into the divider within
				// the XHTML page with the ID of "AllFilms-div".
				$("#AllFilms-div").html(textData(result));
			}
			// This else statement will execute if the format selected is not
			// XML or text.
			else {
				// The jsonData function with a parameter of "result" is called
				// within the HTML method attached to the JQuery call.
				// This returns passes in an JSON document to the jsonData
				// function, which takes the data and converts it to XHTML table
				// data and returns it to the HTML method.
				// This in turn pushes the table data into the divider within
				// the XHTML page with the ID of "AllFilms-div".
				$("#AllFilms-div").html(jsonData(result));
			}
		}
	});
}

// Loaded when the "returnFilmLikeTitle" button is selected.
function loadData(format) {
	// Starts the AJAX call.
	$.ajax({
		// The URL is equal to the "GetFilmLikeTitle" servlet.
		url : "GetFilmLikeTitle",
		// The data is equal to all the data entered into the "getAllFilmsForm"
		data : $("#getFilmByTitleForm").serialize(),
		// If the operation is successful, the anonymous inner function is
		// called and the result of the request is passed into the functions
		// parameter.
		success : function(result) {
			// This is here for testing purposes, to show the result of the
			// operation.
			console.log(result);
			// This if statement checks to see if the format variable is == XML.
			// If it is equal to XML the body of the statement is executed.
			if (format == "xml") {
				// The xmlData function with a parameter of "result" is called
				// within the HTML method attached to the JQuery call.
				// This returns passes in an XML document to the xmlData
				// function, which takes the data and converts it to XHTML table
				// data and returns it to the HTML method.
				// This in turn pushes the table data into the divider within
				// the XHTML page with the ID of "FilmByTitle-div".
				$("#FilmByTitle-div").html(xmlData(result));
			}
			// This else if statement checks to see if the format variable is ==
			// text. If it is equal to text the body of the statement is
			// executed.
			else if (format == "text") {
				// The textData function with a parameter of "result" is called
				// within the HTML method attached to the JQuery call.
				// This returns passes in an text document to the textData
				// function, which takes the data and converts it to XHTML table
				// data and returns it to the HTML method.
				// This in turn pushes the table data into the divider within
				// the XHTML page with the ID of "FilmByTitle-div".
				$("#FilmByTitle-div").html(textData(result));
			}
			// This else statement will execute if the format selected is not
			// XML or text.
			else {
				// The jsonData function with a parameter of "result" is called
				// within the HTML method attached to the JQuery call.
				// This returns passes in an JSON document to the jsonData
				// function, which takes the data and converts it to XHTML table
				// data and returns it to the HTML method.
				// This in turn pushes the table data into the divider within
				// the XHTML page with the ID of "FilmByTitle-div".
				$("#FilmByTitle-div").html(jsonData(result));
			}
		}
	});
}

// This is called if the user selects XML as the format.
function xmlData(result) {
	// Instantiates a blank table variable.
	var table = "";
	// The films variable becomes equal to the children of the result, which is
	// the XML list of film objects.
	var films = result.children[0].children;
	// Runs through the film XML objects and passes each film object into the
	// parameter of the anonymous function,
	// as well as the key which relates to the position of the object in the XML
	// file.
	$.each(films, function(key, film) {
		// The variable header holds the films name with header tags, this is
		// used to show the name of the film in the header div of the film,
		// within the XHTML document.
		var header = '<h3>'
				+ film.getElementsByTagName("name")[0].firstChild.data
				+ '</h3>';
		// The table variable becomes equal to itself and the following XHTML
		// code, which holds the start of the table code and the headers of the
		// table.
		table += '<div class="filmDiv">' + '<div class="filmHeader">' + header
				+ '</div>' + '<div>' + '<table>' + '<tr>' + '<th>' + filmID
				+ '</th>' + '<th>' + filmTitle + '</th>' + '<th>' + filmYear
				+ '</th>' + '<th>' + filmDirector + '</th>' + '<th>'
				+ filmStars + '</th>' + '<th>' + filmReview + '</th>' + '</tr>'
				+ '<tr>';
		// A for each loop is used here to pull the individual elements of the
		// film object into a film-item variable,
		// which is then added between table data tags and to the end of the
		// current table variable.
		// This is done for each of the elements of that particular film.
		$.each(film.children, function(k, film_item) {
			table += '<td>' + film_item.textContent + '</td>';
		});
		// Once the for each loop has finished, the following XHTML tags are
		// added to the end of the table variable,
		// completing the table of information.
		table += '<tr/>' + '</table>' + '</div>' + '</div>' + '<br/>';
	});
	// The table of data is returned.
	return table;
}

// This is called if the user selects no format or JSON as the format.
function jsonData(result) {
	// Instantiates a blank table variable.
	var table = "";
	// This parses the JSON objects as JavaScript objects.
	var films = JSON.parse(result);
	// Loops through the length of the array of JavaScipt objects.
	for (var i = 0; i < films.length; i++) {
		// Pulls the name attribute of the film object and inserts it into the
		// header variable.
		var header = '<h3>' + films[i].name + '</h3>';
		// The table variable becomes equal to itself and the following XHTML
		// code, which holds the start of the table code and the headers of the
		// table.
		table += '<div class="filmDiv">' + '<div class="filmHeader">' + header
				+ '</div>' + '<div>' + '<table>' + '<tr>' + '<th>' + filmID
				+ '</th>' + '<th>' + filmTitle + '</th>' + '<th>' + filmYear
				+ '</th>' + '<th>' + filmDirector + '</th>' + '<th>'
				+ filmStars + '</th>' + '<th>' + filmReview + '</th>' + '</tr>'
				+ '<tr>';
		// The film variable becomes equal to the film object at the current
		// index of the loop.
		var film = films[i];
		// Iterates over each attribute of the film object and if the attribute
		// has a property the if statement is executed.
		for ( var key in film) {
			if (film.hasOwnProperty(key)) {
				// This takes the current attribute of the film object and pulls
				// it into the val variable.
				var val = film[key];
				// the current attribute is then placed within table data tags
				// and added to the end of the table variable.
				table += '<td>' + val + '</td>';
			}
		}

		// Once the film object has been looped through and the attributes of
		// the film are added to table data tags,
		// they are added to the table and the following code is added to the
		// end of the table variable.
		table += '<tr/>' + '</table>' + '</div>' + '</div>' + '<br/>';
	}
	// The XHTML table of data is then returned.
	return table;
}

// This is called if the user selects text as the format.
function textData(result) {
	// Instantiates a blank table variable.
	var table = "";
	// This specifies the delimiter to initially split the results by. This
	// splits the result into an array of films.
	var films = result.split("^");
	// Loops through the length of the films array.
	for (var i = 0; i < films.length - 1; i++) {
		// This specifies the delimiter to split the film by.
		// This splits the film at the current index into its separate
		// attributes and passes the attributes of the film into the film array
		// variable.
		var film = films[i].split("$");
		// The film's name/title is passed into the header variable, in between
		// header tags.
		var header = '<h3>' + film[1] + '</h3>';
		// The table variable becomes equal to itself and the following XHTML
		// code, which holds the start of the table code and the headers of the
		// table.
		table += '<div class="filmDiv">' + '<div class="filmHeader">' + header
				+ '</div>' + '<div>' + '<table>' + '<tr>' + '<th>' + filmID
				+ '</th>' + '<th>' + filmTitle + '</th>' + '<th>' + filmYear
				+ '</th>' + '<th>' + filmDirector + '</th>' + '<th>'
				+ filmStars + '</th>' + '<th>' + filmReview + '</th>' + '</tr>'
				+ '<tr>';
		// Iterates over each attribute of the film object and if the attribute
		// has a property the if statement is executed.
		for ( var key in film) {
			if (film.hasOwnProperty(key)) {
				// This takes the current attribute of the film object and pulls
				// it into the val variable.
				var val = film[key];
				// the current attribute is then placed within table data tags
				// and added to the end of the table variable.
				table += '<td>' + val + '</td>';
			}
		}
		// Once the film object has been looped through and the attributes of
		// the film are added to table data tags,
		// they are added to the table and the following code is added to the
		// end of the table variable.
		table += '<tr/>' + '</table>' + '</div>' + '</div>' + '<br/>';
	}
	// The XHTML table of data is then returned.
	return table;
}

// Loaded when the "insertFilm" button is selected.
function insertData() {
	// Starts the AJAX call.
	$.ajax({
		// The URL is equal to the "InsertFilm" servlet.
		url : "InsertFilm",
		// The data is equal to all the data entered into the "insertFilmForm"
		data : $("#insertFilmForm").serialize(),
		// If the operation is successful, the anonymous inner function is
		// called and the result of the request is passed into the functions
		// parameter.
		success : function(result) {
			// If successful a response of "Film added to database!" will be
			// sent back from the servlet.
			// If unsuccessful a response of "Missing entries in one or many
			// fields" will be sent back from the servlet.
			$("#FilmInsertNotify-div").text(result);
		}
	});
}

// Loaded when the "deleteFilm" button is selected.
function deleteData() {
	// Starts the AJAX call.
	$.ajax({
		// The URL is equal to the "DeleteFilm" servlet.
		url : "DeleteFilm",
		// The data is equal to all the data entered into the "deleteFilmForm"
		data : $("#deleteFilmForm").serialize(),
		// If the operation is successful, the anonymous inner function is
		// called and the result of the request is passed into the functions
		// parameter.
		success : function(result) {
			// If successful a response of "Film deleted from database!" will be
			// sent back from the servlet.
			// If unsuccessful a response of "Missing entries in one or many
			// fields OR Film does not exist" will be sent back from the
			// servlet.
			$("#FilmDeleteNotify-div").text(result);
		}
	});
}

// Loaded when the "updateFilmName" button is selected.
function updateFilmName() {
	// Starts the AJAX call.
	$.ajax({
		// The URL is equal to the "UpdateFilmName" servlet.
		url : "UpdateFilmName",
		// The data is equal to all the data entered into the
		// "updateNameFilmForm"
		data : $("#updateNameFilmForm").serialize(),
		// If the operation is successful, the anonymous inner function is
		// called and the result of the request is passed into the functions
		// parameter.
		success : function(result) {
			// If successful a response of "Film updated!" will be sent back
			// from the servlet.
			// If unsuccessful a response of "Missing entries in one or many
			// fields OR Film not found" will be sent back from the servlet
			$("#FilmUpdateNameNotify-div").text(result);
		}
	});
}
