package coreClasses;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The purpose of this class is to construct, modify and return a film object.
 * 
 * @author Paul Martin
 * @version 1.0
 */

// Root element of film object. This information is used within the xml.jsp
// file.
@XmlRootElement(name = "film")
// This accessor type means that every non static, non transient field in a
// JAXB-bound class will be automatically bound to XML.
@XmlAccessorType(XmlAccessType.FIELD)

public class Film implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private int year;
	private String director;
	private String stars;
	private String review;

	// Film Constructor
	/**
	 * Constructor method for constructing a film object. The parameters are
	 * assigned to the relevant instance variables within the method body.
	 * 
	 * @param id
	 *            Constructs the id of the film object.
	 * @param name
	 *            Constructs the name of the film object.
	 * @param year
	 *            Constructs the year of the film object.
	 * @param director
	 *            Constructs the director of the film object.
	 * @param stars
	 *            Constructs the stars of the film object.
	 * @param review
	 *            Constructs the review of the film object.
	 */
	public Film(int id, String name, int year, String director, String stars, String review) {
		this.id = id;
		this.name = name;
		this.year = year;
		this.director = director;
		this.stars = stars;
		this.review = review;
	}

	/**
	 * No argument film constructor.
	 */

	public Film() {

	}

	/**
	 * Method for returning the id of the film object
	 * 
	 * @return Returns the value of the id variable
	 */
	// Film ID
	public int getId() {
		return id;
	}

	/**
	 * Method for modifying the id of the film object
	 * 
	 * @param FilmIDSet
	 *            Accepts an int datatype input from the user and sets the id
	 *            variable to the new value
	 */
	public void setId(int FilmIDSet) {
		this.id = FilmIDSet;
	}

	// Film Name
	/**
	 * Method for returning the name of the film object
	 * 
	 * @return Returns the value of the name variable
	 */
	public String getName() {
		return name;
	}

	/**
	 * Method for modifying the name of the film object
	 * 
	 * @param FilmNameSet
	 *            Accepts a String datatype input from the user and sets the
	 *            name variable to the new value
	 */
	public void setName(String FilmNameSet) {
		this.name = FilmNameSet;
	}

	// Film Year
	/**
	 * Method for returning the year of the film object
	 * 
	 * @return Returns the value of the year variable
	 */
	public int getYear() {
		return year;
	}

	/**
	 * Method for modifying the year of the film object
	 * 
	 * @param FilmYearSet
	 *            Accepts an int datatype input from the user and sets the year
	 *            variable to the new value
	 */
	public void setYear(int FilmYearSet) {
		this.year = FilmYearSet;
	}

	// Film Director
	/**
	 * Method for returning the director of the film object
	 * 
	 * @return Returns the value of the director variable
	 */
	public String getDirector() {
		return director;
	}

	/**
	 * Method for modifying the director of the film object
	 * 
	 * @param FilmDirectorSet
	 *            Accepts a String datatype input from the user and sets the
	 *            director variable to the new value
	 */
	public void setDirector(String FilmDirectorSet) {
		this.director = FilmDirectorSet;
	}

	// Film Stars
	/**
	 * Method for returning the stars of the film object
	 * 
	 * @return Returns the value of the stars variable
	 */
	public String getStars() {
		return stars;
	}

	/**
	 * Method for modifying the stars of the film object
	 * 
	 * @param FilmStarsSet
	 *            Accepts an String datatype input from the user and sets the
	 *            stars variable to the new value
	 */
	public void setStars(String FilmStarsSet) {
		this.stars = FilmStarsSet;
	}

	// Film Review
	/**
	 * 
	 * Method for returning the review of the film object
	 * 
	 * @return Returns the value of the review variable
	 */
	public String getReview() {
		return review;
	}

	/**
	 * Method for modifying the review of the film object
	 * 
	 * @param FilmReviewSet
	 *            Accepts an String datatype input from the user and sets the
	 *            review variable to the new value
	 */
	public void setReview(String FilmReviewSet) {
		this.review = FilmReviewSet;
	}

	/**
	 * Method for returning a string representation of the film object,
	 * including the delimiter '$' to split each attribute and the delimiter '^'
	 * to split each film.
	 * 
	 * @return - Returns a string which includes the values of the variables
	 *         from the film object and the delimiters.
	 */
	public String toStringAjax() {
		return id + "$" + name + "$" + year + "$" + director + "$" + stars + "$" + review + "^";
	}

	/**
	 * Method for returning a string representation of the film object
	 * 
	 * @return - Returns a string which includes a description of the variables
	 *         and the values of the variables from the film object.
	 */
	public String toString() {
		return "Film ID: " + id + ", Film Name: " + name + ", Year of release: " + year + ", Director: " + director
				+ ", The Stars: " + stars + ", The Review: " + review;
	}
}
