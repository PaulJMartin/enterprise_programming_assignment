package coreClasses;

import java.util.ArrayList;
import java.sql.*;

/**
 * This class holds methods which allows a user to get a single instance of the
 * class through the implementation of the singleton design pattern, create a
 * connection to an MySQL database found on a server (AWS Elastic Beanstalk) and
 * methods which allows the user to access and manipulate the data within the
 * database.
 * 
 * @author Paul Martin 17097016
 */
public class FilmDAO {
	Connection conn = null;
	Statement stment = null;
	Film film = null;
	PreparedStatement preparedStmt = null;

	// Instance object of the FilmDAO class. This is an empty static object
	// which is called upon throughout the application.
	private static FilmDAO singletonFdao;

	/**
	 * This is a private empty constructor which can't be called outside of this
	 * class.
	 */
	private FilmDAO() {
	}

	/**
	 * This is a public static synchronised method which allows the user to get
	 * the single instance of the FilmDAO class. If there isn't one already
	 * instantiated, an instance is instantiated. If one is already
	 * instantiated, it returns that instance when called upon. The method is
	 * synchronised as the applications needs to ensure that the method is not
	 * called at the same time by multiple threads or classes.
	 * 
	 * @return Returns the instance of the FilmDAO object.
	 */

	public static synchronized FilmDAO getSingletonFdao() {
		// if the singletonFdao object == null, a new instance of the FilmDAO is
		// instantiated. A confirmation is also sent back to the console.
		if (singletonFdao == null) {
			singletonFdao = new FilmDAO();
			System.out.println("New FilmDAO object created");
		}
		// If the instance has already been already instantiated, a confirmation
		// of
		// this is sent to the console, and the singletonFdao object is
		// returned.
		System.out.println("FilmDAO object already created");
		return singletonFdao;
	}

	/**
	 * This method ensures that the user cannot clone a FilmDAO object, this is
	 * done by throwing a CloneNotSupportedException if this method is called.
	 */
	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}

	/**
	 * Method for creating the connection to the database. When this method is
	 * called, the database driver is loaded, the connection to the database is
	 * created and a SQL statement is created.
	 */
	private void getConnection() {
		// Loading the JDBC driver for mySQL
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		} catch (Exception e) {
			System.out.println(e);
		}

		// Connection to the database

		try {
			// Database connection to MySQL database on the Amazon Web Service
			// cloud.
			
			 String dbURL = "jdbc:mysql://aar85o9jjlouax.chcxbwdn5jyh.us-east-2.rds.amazonaws.com:3306/ebdb?user=&password=";
			

			// The URL of the database is passed into the parameter of the
			// getConnection() method on the DriverManager. This is then passed
			// into the connection object.
			conn = DriverManager.getConnection(dbURL);
			// The createStatement method is then called on the connection,
			// which creates a statement object for sending SQL statements to
			// the database.
			// This is then passed into the statement object.
			stment = conn.createStatement();
			// Catches all possible exceptions.
		} catch (SQLException se) {
			System.out.println(se);
		}

	}

	/**
	 * Method for closing the connection to the database
	 */
	private void closeConnection() {
		try {
			// The close() method is called on the connection object, which
			// closes the connection.
			conn.close();
			// Catches all possible exceptions.
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method takes in a resultSet object passed in by the SQL return
	 * methods. A film object is instantiated and the attributes of the
	 * resultSet are passed into the film object. Finally the film object is
	 * returned.
	 * 
	 * @param rs
	 *            A resultSet object is passed into the method.
	 * @return Returns a film object.
	 */
	private Film getNextFilm(ResultSet rs) {
		Film nextFilm = null;
		try {
			nextFilm = new Film();
			// The resultSet values are passed into the "nextFilm" film object.
			nextFilm.setId(rs.getInt("id"));
			nextFilm.setName(rs.getString("title"));
			nextFilm.setYear(rs.getInt("year"));
			nextFilm.setDirector(rs.getString("director"));
			nextFilm.setStars(rs.getString("stars"));
			nextFilm.setReview(rs.getString("review"));

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return nextFilm;
	}

	/**
	 * Method for returning all the films from the films table, using a 'SELECT
	 * * FROM films' SQL statement. After executing the statement, the result is
	 * passed into a resultSet object and a while loop is used to loop through
	 * the result set. The result set is then passed into the parameter of the
	 * getNextFilm and the returned film is added into the array list and the
	 * array list is returned.
	 * 
	 * @return returns an array list of film objects.
	 * @throws SQLException
	 *             This exception catches an SQLException and informs the user
	 *             that there has been an error when retrieving all the
	 *             filmsfrom the database.
	 */
	public ArrayList<Film> getAllFilms() throws SQLException {
		// stment.setQueryTimeout(1000);
		ArrayList<Film> allFilms = new ArrayList<Film>();
		Film film = null;
		// SQL statement.
		String sqlStm = "SELECT * FROM films;";
		// Prints the SQL statement to the console for debugging.
		System.out.println(sqlStm);
		try {
			// The connection to the database is opened.
			getConnection();
			// Executes the string representation of the SQL query on the
			// database and pulls the results of the query into the ResultSet
			// object.
			ResultSet rSet = stment.executeQuery(sqlStm);
			// Iterates through the ResultSet and while != Null the resultSet
			// object is sent to the getNextFilm method and a film object is
			// returned.
			while (rSet.next()) {
				film = getNextFilm(rSet);
				// Tests to see if the film object now holds the database tuple
				// and prints the object to the console.
				System.out.println(film.toString());
				// This film object is added to the array list.
				allFilms.add(film);
			}
			// The statement object is closed.
			stment.close();
			// The closeConnection method is called, which closes the connection
			// to the database.
			closeConnection();
			// Catches any possible exception.
		} catch (SQLException e) {
			System.out.println(e);
		}
		// The array list is returned.
		return allFilms;

	}

	/**
	 * Method for returning an array list of films which contain the String
	 * entered by the user in their name. The films are selected by their title
	 * using a SQL WHERE LIKE clause. Prepared statements are used here to stop
	 * an SQL injection, as this method takes in input from a user on the
	 * front-end, which could leave the application open to attack.
	 * 
	 * @param title
	 *            Accepts a string input from the user in the form of an film
	 *            name and uses the string value entered, within the Where like
	 *            clause of the SQL SELECT statement.
	 * @return Returns an array list of film objects.
	 * @throws SQLException
	 *             This exception catches an SQLException and informs the user
	 *             that there has been an error when retrieving the films from
	 *             the database.
	 */
	public ArrayList<Film> getFilmLikeTitle(String title) throws SQLException {
		ArrayList<Film> filmsByWhere = new ArrayList<Film>();
		Film film = null;
		preparedStmt = null;
		// String format of the SQL statement.
		String sqlStm = "SELECT * FROM films WHERE title LIKE ?";
		// Prints the SQL statement to the console for debugging.
		System.out.println(sqlStm);
		try {
			// The connection to the database is opened.
			getConnection();
			// The connection object invokes the prepared statement method with
			// the SQL string as a parameter. The prepared statement object then
			// holds this SQL statement.
			preparedStmt = conn.prepareStatement(sqlStm);
			// Sets the designated parameter (?) to the given Java String value.
			// The first value sets the index of the parameter and the second is
			// the value.
			preparedStmt.setString(1, "%" + title + "%");
			// Prints the prepared statement to the console for debugging.
			System.out.println(preparedStmt);
			// Executes the string representation of the prepared statement on
			// the database and pulls the results of the query into the
			// ResultSet object.
			ResultSet rSet = preparedStmt.executeQuery();
			// Iterating through the ResultSet and while != Null the result is
			// converted into a Film object and added into the array list.
			while (rSet.next()) {
				film = getNextFilm(rSet);
				// Tests to see if the film object now holds the database tuple
				// and prints the object to the console.
				System.out.println(film.toString());
				// The film object is added to the array list.
				filmsByWhere.add(film);
			}
			// Catches any possible exception.
		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			// If the prepared statement object is not null, the prepared
			// statement object's database and JDBC resources are released
			// immediately.
			if (preparedStmt != null) {
				preparedStmt.close();
			}
			// If the connection object is not null, the connection is closed.
			if (conn != null) {
				closeConnection();
			}
		}
		// The array list is returned.
		return filmsByWhere;
	}

	/**
	 * Method for returning a specific film. The film is selected by its title
	 * using a SQL WHERE clause. Prepared statements are used here to stop an
	 * SQL injection, as this method takes in input from a user on the
	 * front-end, which could leave the application open to attack.
	 * 
	 * @param title
	 *            Accepts a string input from the user in the form of an film
	 *            name and uses the string value within the Where clause of the
	 *            SQL SELECT statement.
	 * @return Returns the selected film from the film table.
	 * @throws SQLException
	 *             This exception catches an SQLException and informs the user
	 *             that there has been an error when retrieving a specific film
	 *             from the database.
	 */
	public ArrayList<Film> getFilmByTitle(String title) throws SQLException {
		ArrayList<Film> filmsByWhere = new ArrayList<Film>();
		Film film = null;
		// String format of the SQL statement.
		String sqlStm = "SELECT * FROM films WHERE title = ?";
		// Prints the SQL statement to the console for debugging.
		System.out.println(sqlStm);
		try {
			// The connection to the database is opened.
			getConnection();
			// The connection object invokes the prepared statement method with
			// the SQL string as a parameter. The prepared statement object then
			// holds this SQL statement.
			preparedStmt = conn.prepareStatement(sqlStm);
			// Sets the designated parameter (?) to the given Java String value.
			// The first value sets the index of the parameter and the second is
			// the value.
			preparedStmt.setString(1, title);
			// Prints the prepared statement to the console for debugging.
			System.out.println(preparedStmt);
			// Executes the string representation of the prepared statement on
			// the database and pulls the results of the query into the
			// ResultSet object.
			ResultSet rSet = preparedStmt.executeQuery();
			// Iterating through the ResultSet and while != Null the result is
			// converted into a Film object and added into the array list.
			while (rSet.next()) {
				film = getNextFilm(rSet);
				// Tests to see if the film object now holds the database tuple
				// and prints the object to the console.
				System.out.println(film.toString());
				// The film object is added to the array list.
				filmsByWhere.add(film);
			}
			// Catches any possible exception.
		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			// If the prepared statement object is not null, the prepared
			// statement object's database and JDBC resources are released
			// immediately.
			if (preparedStmt != null) {
				preparedStmt.close();
			}
			// If the connection object is not null, the connection is closed.
			if (conn != null) {
				closeConnection();
			}
		}
		// The array list is returned.
		return filmsByWhere;
	}

	/**
	 * Method for entering a new film into the database, the new film is passed
	 * into the parameter of the method when the method is called and get/set
	 * methods are used to pull the data into the prepared statement. Prepared
	 * statements are used here to stop an SQL injection, as this method takes
	 * in input from a user on the front-end, which could leave the application
	 * open to attack.
	 * 
	 * @param f
	 *            Accepts a film object input from the user and uses the film
	 *            object values within the INSERT statement.
	 * @return Returns a String of 1 if the operation is successful and 0 if
	 *         not. The success codes are also printed to the console.
	 * @throws SQLException
	 *             This exception catches an SQLException and informs the user
	 *             that there has been an error when inserting a new film
	 *             intothe database.
	 */
	public String insertFilm(Film f) throws SQLException {
		// preparedStmt.setQueryTimeout(1000);
		String success = "1";
		String unsuccessful = "0";
		preparedStmt = null;
		// String format of the SQL statement.
		String sqlStm = "INSERT into films (id, title, year,  director, stars, review) VALUES(?, ?, ?, ?, ?, ?)";
		// Prints the SQL statement to the console for debugging.
		System.out.println(sqlStm);
		try {
			// The connection to the database is opened.
			getConnection();
			// The connection object invokes the prepared statement method with
			// the SQL string as a parameter. The prepared statement object then
			// holds this SQL statement.
			preparedStmt = conn.prepareStatement(sqlStm);
			// Sets the designated parameter (?) to the given Java int or String
			// value. The first value sets the index of the parameter and the
			// second is the value.
			preparedStmt.setInt(1, f.getId());
			preparedStmt.setString(2, f.getName());
			preparedStmt.setInt(3, f.getYear());
			preparedStmt.setString(4, f.getDirector());
			preparedStmt.setString(5, f.getStars());
			preparedStmt.setString(6, f.getReview());
			// Prints the prepared statement to the console for debugging.
			System.out.println(preparedStmt);
			// The prepared statement is executed on the database, which inserts
			// a new film.
			preparedStmt.executeUpdate();
			// Catches any possible exception.
		} catch (SQLException sqle) {
			System.out.println(sqle);
		} catch (Exception e) {
			e.printStackTrace();
			// If the insert was unsuccessful the success code of 0 is returned
			// and also printed to the console.
			System.out.println("Insert operation unsuccessful - Success code: 0");
			System.out.println();
			return unsuccessful;
		} finally {
			// If the prepared statement object is not null, the prepared
			// statement object's database and JDBC resources are released
			// immediately.
			if (preparedStmt != null) {
				preparedStmt.close();
			}
			// If the connection object is not null, the connection is closed.
			if (conn != null) {
				closeConnection();
			}

		}
		// If the insert was successful the success code of 1 is returned and
		// also printed to the console.
		System.out.println("Insert operation successful - Success code: 1");
		System.out.println();
		return success;

	}

	/**
	 * Method for deleting a specific film. The film to be deleted is selected
	 * by their ID using a SQL WHERE clause. Prepared statements are used here
	 * to stop an SQL injection, as this method takes in input from a user on
	 * the front-end, which could leave the application open to attack.
	 * 
	 * @param ID
	 *            Accepts an int input from the user in the form of an ID and
	 *            uses the int value within the Where clause of the SQL DELETE
	 *            statement.
	 * @return Returns a String of 1 if the operation is successful and 0 if
	 *         not. The success codes are also printed to the console.
	 * @throws SQLException
	 *             This exception catches an SQLException and informs the user
	 *             that there has been an error when inserting a new film into
	 *             the database.
	 */
	public String deleteFilmById(int ID) throws SQLException {
		String success = "1";
		String unsuccessful = "0";
		preparedStmt = null;
		// String format of the SQL statement.
		String sqlStm = "DELETE FROM films WHERE id = ?";
		// Prints the SQL statement to the console for debugging.
		System.out.println(sqlStm);
		try {
			// The connection to the database is opened.
			getConnection();
			// The connection object invokes the prepared statement method with
			// the SQL string as a parameter. The prepared statement object then
			// holds this SQL statement.
			preparedStmt = conn.prepareStatement(sqlStm);
			// Sets the designated parameter (?) to the given Java int value.
			// The first value sets the index of the parameter and the second is
			// the value.
			preparedStmt.setInt(1, ID);
			// Prints the prepared statement to the console for debugging.
			System.out.println(preparedStmt);
			// The prepared statement is executed on the database, which deletes
			// the film.
			preparedStmt.executeUpdate();
			// Catches any possible exception.
		} catch (Exception e) {
			e.printStackTrace();
			// If the delete was unsuccessful the success code of 0 is returned
			// and also printed to the console.
			System.out.println("Delete operation unsuccessful - Success code: 0");
			System.out.println();
			return unsuccessful;
		} finally {
			// If the prepared statement object is not null, the prepared
			// statement object's database and JDBC resources are released
			// immediately.
			if (preparedStmt != null) {
				preparedStmt.close();
			}
			// If the connection object is not null, the connection is closed.
			if (conn != null) {
				closeConnection();
			}
		}
		// If the delete was successful the success code of 1 is returned and
		// also printed to the console.
		System.out.println("Delete operation successful - Success code: 1");
		return success;
	}

	/**
	 * Method for updating the name of a specific film. The film to be updated
	 * is selected by their id using a SQL WHERE clause. Prepared statements are
	 * used here to stop an SQL injection, as this method takes in input from a
	 * user on the front-end, which could leave the application open to attack.
	 * 
	 * @param feildUpdate
	 *            Accepts a string input from the user in the form of the new
	 *            name value. The string is used to update the films name.
	 * @param Id
	 *            Accepts an int input from the user in the form of an film id
	 *            and uses the int value within the Where clause of the SQL
	 *            UPDATE statement.
	 * @return Returns a String of 1 if the operation is successful and 0 if
	 *         not. The success codes are also printed to the console.
	 * @throws SQLException
	 *             This exception catches an SQLException and informs the user
	 *             that there has been an error when updating the film in the
	 *             database.
	 */
	public String updateFilmName(String feildUpdate, int Id) throws SQLException {
		// preparedStmt.setQueryTimeout(1000);
		String success = "1";
		String unsuccessful = "0";
		preparedStmt = null;
		// String format of the SQL statement.
		String sqlStm = "UPDATE films SET title = ? WHERE id = ?";
		// Prints the SQL statement to the console for debugging.
		System.out.println(sqlStm);
		try {
			// The connection to the database is opened.
			getConnection();
			// The connection object invokes the prepared statement method with
			// the SQL string as a parameter. The prepared statement object then
			// holds this SQL statement.
			preparedStmt = conn.prepareStatement(sqlStm);
			// Sets the designated parameter (?) to the given Java int or String
			// value. The first value sets the index of the parameter and the
			// second is the value.
			preparedStmt.setString(1, feildUpdate);
			preparedStmt.setInt(2, Id);
			// Prints the prepared statement to the console for debugging.
			System.out.println(preparedStmt);
			// The prepared statement is executed on the database, which updates
			// the film.
			preparedStmt.executeUpdate();
			// Catches any possible exception
		} catch (Exception e) {
			e.printStackTrace();
			// If the update was unsuccessful the success code of 0 is returned
			// and also printed to the console.
			System.out.println("Update operation unsuccessful - Success code: 0");
			System.out.println();
			return unsuccessful;
		} finally {
			// If the prepared statement object is not null, the prepared
			// statement object's database and JDBC resources are released
			// immediately.
			if (preparedStmt != null) {
				preparedStmt.close();
			}
			// If the connection object is not null, the connection is closed.
			if (conn != null) {
				closeConnection();
			}
		}
		// If the update was successful the success code of 1 is returned and
		// also printed to the console.
		System.out.println("Update operation successful - Success code: 1");
		System.out.println();
		return success;
	}
}
