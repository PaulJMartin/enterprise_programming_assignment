package coreClasses;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

// Root element of the films object. This information is used within the xml.jsp file.
@XmlRootElement(name = "filmlist")
// This accessor type means that every non static, non transient field in a
// JAXB-bound class will be automatically bound to XML.
@XmlAccessorType(XmlAccessType.FIELD)

/**
 * This is a container class to hold a list of film objects. When the data is
 * converted to xml, the films and their attributes will be placed within the
 * films root element.
 * 
 * @author Paul Martin 17097016
 *
 */
public class FilmList {

	private List<Film> films = null;

	/**
	 * A list of films is passed into this constructor when the FilmList object
	 * is created. The parameter is then assigned to the relevant private
	 * instance variable within the method body.
	 * 
	 * @param inFilms
	 */
	public FilmList(List<Film> inFilms) {
		this.films = inFilms;
	}

	/**
	 * Empty FilmList constructor.
	 */
	public FilmList() {
	}

}
