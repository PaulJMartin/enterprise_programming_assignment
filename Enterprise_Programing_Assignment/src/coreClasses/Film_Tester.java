package coreClasses;

import java.util.ArrayList;

/**
 * The purpose of this class is to test all of the methods found within the
 * FilmDAO class except for the getAllFilms method.
 * 
 * @author Paul Martin 17097016
 */
public class Film_Tester {

	/**
	 * All FilmDAO methods are tested within the main method of this class
	 * except for the getAllFilms method, which is tested in a separate class.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// To use the methods of the FilmDAO class I have used the
		// getSingletonFdao() method to gain the singleton instance of this
		// class.
		FilmDAO fDAO = FilmDAO.getSingletonFdao();
		ArrayList<Film> filmExactTitle = new ArrayList<Film>();
		ArrayList<Film> filmLikeTitle = new ArrayList<Film>();
		Film film = null;

		// Inserts a news film
		try {
			film = new Film(12301, "The adventures of Pete the paint", 2015, "Ron Swonson", "Dulux & Robert DeNero",
					"A brilliant coming of age film, which finally grasps the trials and tribulations of the life of paint."
							+ " The way it shows the emotional journey paint goes through from wet to dry is breathtaking");
			fDAO.insertFilm(film);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Gets a film by the title "The adventures of Pete the paint" and
		// prints the film object to the console.
		try {
			filmExactTitle = fDAO.getFilmByTitle("The adventures of Pete the paint");
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(filmExactTitle);
		System.out.println();

		// Updates the film name from "The adventures of Pete the paint" to "The
		// adventures of Pete the paint, Part 2".
		try {
			fDAO.updateFilmName("The adventures of Pete the paint, Part 2", 12301);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Gets a film by the title "The adventures of Pete the paint, Part 2"
		// and prints the film object to the console.
		try {
			filmExactTitle = fDAO.getFilmByTitle("The adventures of Pete the paint, Part 2");
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(filmExactTitle);
		System.out.println();

		// Deletes the newly added film, identified by its ID.
		try {
			fDAO.deleteFilmById(12301);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/*
		 * Attempts to get the film by the title
		 * "The adventures of Pete the paint, Part 2". This should return no
		 * records, as the film should have been deleted from the database.
		 */
		try {
			filmExactTitle = fDAO.getFilmByTitle("The adventures of Pete the paint, Part 2");
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(filmExactTitle);
		System.out.println();

		// Gets all films with the word "Alien" in its name and prints the film
		// objects to the console.

		try {
			filmLikeTitle = fDAO.getFilmLikeTitle("Alien");
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(filmLikeTitle);
		System.out.println();
	}
}
