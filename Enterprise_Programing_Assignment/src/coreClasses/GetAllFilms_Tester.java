package coreClasses;

import java.util.ArrayList;

import com.google.gson.Gson;

/**
 * The purpose of this class is to test the getAllFilms method of the FilmDAO
 * class.
 * 
 * @author Paul Martin 17097016
 */
public class GetAllFilms_Tester {

	/**
	 * The getAllFilms method is tested within the main method of this class
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// To use the methods of the FilmDAO class I have used the
		// getSingletonFdao() method to gain the singleton instance of this
		// class.
		FilmDAO fDAO = FilmDAO.getSingletonFdao();
		ArrayList<Film> allFilms = new ArrayList<Film>();
		Gson gson = new Gson();

		System.out.println("Retrieving all employees from the database: ");
		// Pulls all the films from the database and passes the list of film
		// objects into an array list.
		try {
			allFilms = fDAO.getAllFilms();
			// Catches any possible Exception
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Loops through the array list and each film object in the allFilms
		// array list is passed into a film object.
		// The film object is then passed into the parameter of the Gson object
		// with a method of toJson, which parses the film object as a Json
		// String.
		// The Json film string is then printed to the console.
		for (Film film : allFilms) {
			String intoJson = gson.toJson(film);
			System.out.println(intoJson);
		}

		System.out.println();
	}
}
