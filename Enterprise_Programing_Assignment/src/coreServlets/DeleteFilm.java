package coreServlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import coreClasses.FilmDAO;

/**
 * Servlet implementation class DeleteFilm This is the DeleteFilm servlet, which
 * receives input from the user on the front-end, in the form of a film ID. This
 * data is then used by the Film DAO instance to delete a particular record
 * within the database, by using the films ID as an indicator The user is then
 * informed on the successfulness of the operation.
 * 
 * @author Paul Martin 17097016
 */
// The web servlet annotation below is used to declare the servlet.
@WebServlet("/DeleteFilm")
public class DeleteFilm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * This method receives a single parameter from the user in the form of the
	 * films ID, this is then parsed as an int to follow the rules of the Film
	 * class and placed into its corresponding variable. The variable is then
	 * passed into the parameter of the deleteFilmById method on the FilmDAO
	 * instance. This then deletes the film with the corresponding ID value, if
	 * one exists. The user on the front end and the console is informed on
	 * whether or not the operation was a success.
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// This tells the browser not to cache, as this would stop the
		// application from refreshing.
		response.setHeader("Cache-Control", "no-cache");
		// This header with a value of no-cache instructs HTTP 1.0 clients not
		// to cache the document
		response.setHeader("Pragma", "no-cache");
		// To use the methods of the FilmDAO class I have used the
		// getSingletonFdao() method to gain the singleton instance of this
		// class.
		FilmDAO fDAO = FilmDAO.getSingletonFdao();

		try {
			// Pulls the data from the text input with the id of fID.
			// The data is pulled in as String data, but the film objects ID
			// parameter is an int datatype.
			// This means that the data needs to be parsed as an int and then
			// parsed into an int variable.
			int filmID = Integer.parseInt(request.getParameter("fID"));
			// The filmID variable is then passed into the parameter of the
			// deleteFilmById method on the FilmDAO instance.
			// This method then uses the film id to locate the film to be
			// deleted and then deletes it from the database, if the film with
			// that id is found.
			fDAO.deleteFilmById(filmID);
			// A response of "Film deleted from database!" is then sent to the
			// front-end to inform the user that the deletion was successful.
			response.getWriter().println("Film deleted from database!");
			// A delete result of "1" is then printed to the console, to inform
			// the user that the deletion was successful.
			System.out.println("Delete result = 1");

			// Catches any possible exceptions.
		} catch (NullPointerException npe) {
			npe.printStackTrace();
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
			// A response of "Missing entries in one or many fields OR Film does
			// not exist" is then sent to the front-end
			// to inform the user that the deletion was unsuccessful / there are
			// missing fields.
			response.getWriter().println("Missing entries in one or many fields OR Film does not exist");
			// A delete result of "0" and "Missing entries in one or many fields
			// OR Film does not exist" is then printed to the console,
			// to inform the user that the deletion was unsuccessful.
			System.out.println("Delete Result = 0");
			System.out.println("Missing entries in one or many fields OR Film does not exist");
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	/**
	 * The doPost method is not currently used.
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
