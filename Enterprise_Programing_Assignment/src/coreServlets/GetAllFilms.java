package coreServlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import coreClasses.Film;
import coreClasses.FilmDAO;

/**
 * Servlet implementation class GetAllFilms This is the GetAllFilms servlet,
 * which receives a request from the client for all film objects. The
 * getAllFilms method is called on the singleton Instance of the FilmDAO class,
 * this pulls back all the films from the database. The format chosen by the
 * user is then checked and the array list of films is sent to the relevant JSP
 * file. The data is then converted into the relevant format and sendt back to
 * the server. Finally the server sends the raw data to the AJAX client on the
 * front-end. The user is then informed on the successfulness of the operation.
 * 
 * @author Paul Martin 17097016
 */
// The web servlet annotation below is used to declare the servlet.
@WebServlet("/GetAllFilms")
public class GetAllFilms extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * This method receives a request from the browser for all film objects and
	 * sets the request attribute of "Films" to the array list of films pulled
	 * from the database. The format parameter entered by the user is pulled
	 * from the front-end into a String variable. The format variable is then
	 * checked against a block of if else statements and the relevant statement
	 * body is then ran depending on the format that user has specified(XML,
	 * JSON, TEXT) Inside the statement body, the content type of the response
	 * is set to the relevant data type, so the client browser knows what type
	 * of data is being sent back. The output page variable is also set to the
	 * location of the relevant JSP file. The RequestDispatcher object
	 * dispatches the request to the current output page(JSP file) which runs
	 * through the array list of restaurant objects, converts the data into the
	 * relevant data type(XML, JSON or TEXT) and sends the response object which
	 * holds the data back to the servlet, which then sends the data to the
	 * front-end application(AJAX file)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// This tells the browser not to cache, as this would stop the
		// application from refreshing.
		response.setHeader("Cache-Control", "no-cache");
		// This header with a value of no-cache instructs HTTP 1.0 clients not
		// to cache the document
		response.setHeader("Pragma", "no-cache");

		// To use the methods of the FilmDAO class I have used the
		// getSingletonFdao() method to gain the singleton instance of this
		// class.
		FilmDAO fDAO = FilmDAO.getSingletonFdao();
		ArrayList<Film> allFilmsS = null;

		// This try block uses the getAllFilms method found within the FilmDAO
		// class to pull all the film objects from the database into an array
		// list of type film.
		try {
			allFilmsS = fDAO.getAllFilms();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		// This try block runs through each index of the allFilmsS array list,
		// pulls the film objects into a film object and then uses the film
		// classes toString method to print the films to the console in String
		// format.
		try {
			for (Film film : allFilmsS) {
				System.out.println(film.toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		// Sets the attribute "Films" to the array list allFilmsS. This
		// attribute is then used by the JSP files to parse the data from the
		// servlet.
		request.setAttribute("Films", allFilmsS);
		// Pulls out the data found within the dropdown list with the id of
		// "format". The String data is then passed to the String variable
		// "format".
		String format = request.getParameter("format");
		String outputPage;

		// Within these statements the data within the String called "format" is
		// checked.

		// If the String "format" contains the String "xml" the body of the if
		// statement is is run.
		if ("xml".equals(format)) {
			// The servlet informs the client browser of what type of data is
			// being sent.
			response.setContentType("text/xml");
			// Sets the output page String variable to the location of the
			// films-xml.jsp file.
			outputPage = "/WEB-INF/results/films-xml.jsp";
		}
		// If the String "format" contains the String "text" the body of the
		// else if statement is is run.
		else if ("text".equals(format)) {
			// The servlet informs the client browser of what type of data is
			// being sent
			response.setContentType("text/plain");
			// Sets the output page String variable to the location of the
			// films-string.jsp file.
			outputPage = "/WEB-INF/results/films-string.jsp";
		}
		// If neither the if or the else if statement is triggered, the body of
		// the else statement is run.
		else {
			// The servlet informs the client browser of what type of data is
			// being sent
			response.setContentType("text/javascript");
			// Sets the output page String variable to the location of the
			// films-json.jsp file.
			outputPage = "/WEB-INF/results/films-json.jsp";
		}

		// Dispatches the request to the current output page(JSP file) which
		// runs through the array list of restaurant objects,
		// converts the data into the relevant data type(XML, JSON or TEXT) and
		// sends the response object which holds the data back to the servlet,
		// which then sends the data to the front-end application(AJAX file)
		RequestDispatcher dispatcher = request.getRequestDispatcher(outputPage);
		dispatcher.include(request, response);

	}

	/**
	 * The doPost method is not currently used.
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
