package coreServlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import coreClasses.Film;
import coreClasses.FilmDAO;

/**
 * Servlet implementation class insertFilm This is the InsertFilm servlet, which
 * receives input from the user on the front-end in the form of all the
 * attributes of the Film class's constructor. It then uses the data to
 * construct a Film object and the film object is used by the Film DAO instance
 * to insert a new record into the database. The user is then informed on the
 * successfulness of the operation.
 * 
 * @author Paul Martin 17097016
 */
// The web servlet annotation below is used to declare the servlet.
@WebServlet("/InsertFilm")
public class InsertFilm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * This method receives six parameters from the user, each parameter
	 * corresponds to an attribute of the film class constructor, these are then
	 * placed into their corresponding variables depending on their type of
	 * data, with the ID and Year variables being parsed as ints, to follow the
	 * rules of the Film constructor. After this an empty Film object is created
	 * and the class's setter methods are used to set the objects attributes to
	 * the corresponding variables. The film object is then passed into the
	 * parameter of the insertFilm method on the FilmDAO instance. This takes
	 * the attributes of the film object and uses them to insert a new film
	 * record into the database. The user on the front end and the console is
	 * informed on whether or not the operation was a success.
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// This tells the browser not to cache, as this would stop the
		// application from refreshing.
		response.setHeader("Cache-Control", "no-cache");
		// This header with a value of no-cache instructs HTTP 1.0 clients not
		// to cache the document
		response.setHeader("Pragma", "no-cache");

		// To use the methods of the FilmDAO class I have used the
		// getSingletonFdao() method to gain the singleton instance of this
		// class.
		FilmDAO fDAO = FilmDAO.getSingletonFdao();

		try {
			// Pulls the data from all the text input fields with the relevant
			// id's, seen in the getParameter methods parameters bellow.
			// The data is then pulled into the adjacent String variables.
			String filmID = request.getParameter("fID");
			String filmName = request.getParameter("fTitle");
			String filmYear = request.getParameter("fYear");
			String filmDirector = request.getParameter("fDirector");
			String filmStars = request.getParameter("fStars");
			String filmReview = request.getParameter("fReview");

			// If all the string variables are not null, the body of the IF
			// statement will run.
			if (filmID != null && filmName != null && filmYear != null && filmDirector != null && filmStars != null
					&& filmReview != null) {
				// A new empty Film object is constructed.
				Film f = new Film();
				// The String variable filmID is parsed as an int and the int
				// variable version is used to set the Film objects ID
				// attribute.
				f.setId(Integer.parseInt(filmID));
				// The filmName variable is used to set the Film objects Name
				// attribute.
				f.setName(filmName);
				// The String variable filmYear is parsed as an int and the int
				// variable version is used to set the Film objects Year
				// attribute.
				f.setYear(Integer.parseInt(filmYear));
				// The filmDirector variable is used to set the Film objects
				// Director attribute.
				f.setDirector(filmDirector);
				// The filmStars variable is used to set the Film objects Stars
				// attribute.
				f.setStars(filmStars);
				// The filmReview variable is used to set the Film objects
				// Review attribute.
				f.setReview(filmReview);

				// The Film object with its new attributes is passed into the
				// parameter of the insertFilm method of the FilmDAO instance.
				// This method uses the film objects attributes to insert a new
				// film record in the database.
				fDAO.insertFilm(f);
				// A response of "Film added to database!" is then sent to the
				// front-end to inform the user that the insertion was
				// successful.
				response.getWriter().println("Film added to database!");
				// An insert result of "1" is then printed to the console, to
				// inform the user that the isert was successful.
				System.out.println("Insert result = 1");

			}

			// If any of the string variables are null, the else statement will
			// run.
			else {
				// A response of "Missing entries in one or many fields" is then
				// sent to the front-end
				// to inform the user that the insert was unsuccessful / there
				// are missing fields.
				response.getWriter().println("Missing entries in one or many fields");
				// An insert result of "0" and "Missing entries in one or many
				// fields" is then printed to the console,
				// to inform the user that the insert was unsuccessful.
				System.out.println("Insert Result = 0");
				System.out.println("Missing entries in one or many fields");
			}

		} catch (NullPointerException npe) {
			npe.printStackTrace();
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
			// A response of "Missing entries in one or many fields" is then
			// sent to the front-end
			// to inform the user that the insert was unsuccessful / there are
			// missing fields.
			response.getWriter().println("Missing entries in one or many fields");
			// A delete result of "0" and "Missing entries in one or many
			// fields" is then printed to the console,
			// to inform the user that the deletion was unsuccessful.
			System.out.println("Insert Result = 0");
			System.out.println("Missing entries in one or many fields");
		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	/**
	 * The doPost method is not currently used.
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
