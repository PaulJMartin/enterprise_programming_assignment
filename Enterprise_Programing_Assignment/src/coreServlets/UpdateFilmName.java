package coreServlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import coreClasses.FilmDAO;

/**
 * Servlet implementation class UpdateFilmName This is the UpdateFilmName
 * servlet, which receives input from the user on the front-end, in the form of
 * a film ID and film name. This data is then used by the Film DAO instance to
 * update a record within the database. The user is then informed on the
 * successfulness of the operation.
 * 
 * @author Paul Martin 17097016
 */
// The web servlet annotation below is used to declare the servlet.
@WebServlet("/UpdateFilmName")
public class UpdateFilmName extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * This method receives two parameters from the user, the films ID and the
	 * new film name, these are then placed into their corresponding variables
	 * depending on their type of data. The variables are then passed into the
	 * parameters of the updateFilmName method on the FilmDAO instance. This
	 * finds the film to be updated using the ID attribute and updates the name
	 * attribute, with the contents of the filmName variable. The user on the
	 * front end and the console is informed on whether or not the operation was
	 * a success.
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// This tells the browser not to cache, as this would stop the
		// application from refreshing.
		response.setHeader("Cache-Control", "no-cache");
		// This header with a value of no-cache instructs HTTP 1.0 clients not
		// to cache the document
		response.setHeader("Pragma", "no-cache");
		// To use the methods of the FilmDAO class I have used the
		// getSingletonFdao() method to gain the singleton instance of this
		// class.
		FilmDAO fDAO = FilmDAO.getSingletonFdao();

		try {
			// Pulls the data from the text input with the id of fID, the string
			// data is then parsed as an int and the data is then pulled into
			// the int variable "filmID"
			int filmID = Integer.parseInt(request.getParameter("fID"));
			// Pulls the data from the text input with the id of fTitle and the
			// data is then pulled into the String variable "filmName"
			String filmName = request.getParameter("fTitle");
			// The filmID and filmName variables are passed into the parameters
			// of the updateFilmName method on the FilmDAO instance.
			// This method then uses the filmID to locate the film that need to
			// be updated and the contents of the filmName variable is the new
			// name attribute of the film.
			fDAO.updateFilmName(filmName, filmID);
			// A response of "Film updated!" is then sent to the front-end to
			// inform the user that the update was successful.
			response.getWriter().println("Film updated!");
			// An update result of "1" is then printed to the console, to inform
			// the user that the update was successful.
			System.out.println("Update result = 1");

		} catch (NullPointerException npe) {
			npe.printStackTrace();
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
			// A response of "Missing entries in one or many fields OR Film not
			// found" is then sent to the front-end
			// to inform the user that the update was unsuccessful / there are
			// missing fields.
			response.getWriter().println("Missing entries in one or many fields OR Film not found");
			// An update result of "0" and "Missing entries in one or many
			// fields OR Film not found" is then printed to the console,
			// to inform the user that the insert was unsuccessful.
			System.out.println("Update Result = 0");
			System.out.println("Missing entries in one or many fields OR Film not found");
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	/**
	 * The doPost method is not currently used.
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
