# Enterprise_Programming_Assignment

Cloud-based film application which allows a user to search for a list of films by name, update the details of a film, insert a new film and finally delete a film from a database, held on the cloud using AWS Elastic Beanstalk along with the application itself.

The application follows the MVC architectural framework with Java in the back-end and an AJAX application in the front end which asynchronously updates the front-end webpage, developed using HTML, CSS and JQuery. 

Design patterns such as the singleton pattern and the DTO pattern are implemented to improve efficiency and reduce resource overload.

Security features have also been implemented in the form of prepared statements to secure against SQL injection. 


*** The application is no longer hosted on the cloud, but the application can be deployed in it's current state. ***